<?php

namespace Drupal\salesforce\Plugin;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\salesforce\SalesforceApi;
use Drupal\salesforce\SalesforceSync;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Salesforce mapping plugin manager.
 */
class SalesforceMappingManager extends DefaultPluginManager {

  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * Constructor for SalesforceMappingManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ContainerInterface $container) {
    parent::__construct('Plugin/SalesforceMapping', $namespaces, $module_handler, 'Drupal\salesforce\Plugin\SalesforceMapping\SalesforceMappingInterface', 'Drupal\salesforce\Annotation\SalesforceMapping');

    $this->alterInfo('salesforce_salesforce_mapping_info');
    $this->setCacheBackend($cache_backend, 'salesforce_salesforce_mapping_plugins');
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   * @return \Drupal\salesforce\Plugin\SalesforceMapping\SalesforceMappingBase
   */
  public function createInstance($plugin_id, array $configuration = array()) {
    /** @var \Drupal\salesforce\Plugin\SalesforceMapping\SalesforceMappingBase $plugin */
    $plugin = parent::createInstance($plugin_id, $configuration);
    $plugin->setSalesforceApi($this->container->get('salesforce_api'))
      ->setSalesforceSync($this->container->get('salesforce_sync'))
      ->setEntityTypeManager($this->container->get('entity_type.manager'))
      ->setCacheBackend($this->container->get('cache.default'));
    return $plugin;
  }

  /**
   * Creates a new plugin instance for the specific entity type.
   *
   * @param string $entity_type_id
   * @param array $configuration
   * @return \Drupal\salesforce\Plugin\SalesforceMapping\SalesforceMappingBase|null
   */
  public function createInstanceForEntityType($entity_type_id, array $configuration = []) {
    if ($plugin_id = $this->findPluginForEntityType($entity_type_id)) {
      return $this->createInstance($plugin_id, $configuration);
    }

    return null;
  }

  /**
   * Creates a new plugin instance for the specific salesforce object.
   *
   * @param string $salesforce_object
   * @param array $configuration
   * @return \Drupal\salesforce\Plugin\SalesforceMapping\SalesforceMappingBase|null
   */
  public function createInstanceForSalesforceObject($salesforce_object, array $configuration = []) {
    if ($plugin_id = $this->findPluginForSalesforceObject($salesforce_object)) {
      return $this->createInstance($plugin_id, $configuration);
    }

    return null;
  }

  /**
   * Gets plugin id by entity type.
   *
   * @param string $entity_type_id
   * @return string|null
   */
  public function findPluginForEntityType($entity_type_id) {
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if ($definition['entity_type_id'] == $entity_type_id) {
        return $plugin_id;
      }
    }

    return null;
  }

  /**
   * Gets plugin id by salesforce object.
   *
   * @param string $salesforce_object
   * @return string|null
   */
  public function findPluginForSalesforceObject($salesforce_object) {
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if ($definition['salesforce_object'] == $salesforce_object) {
        return $plugin_id;
      }
    }

    return null;
  }

}
