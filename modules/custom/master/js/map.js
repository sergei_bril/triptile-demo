/**
 * @file
 * Attaches behaviors for the Selectize.js module.
 */

(function ($, Drupal, drupalSettings) {

    'use strict';

    /**
     * @type {Drupal~behavior}
     *
     * @prop {Drupal~behaviorAttach} attach
     *   Attaches behavior to the page from defined settings for selectize to each specified element.
     */
    Drupal.behaviors.map = {
        attach: function (context) {
            if (typeof drupalSettings.stations != 'undefined') {
                var stations = drupalSettings.stations;
                if (stations['departure_station'].lat)
                L.mapbox.accessToken = 'pk.eyJ1IjoidHJhdmVsYWxscnVzc2lhIiwiYSI6IjA1Q2F1S3cifQ.BQwNPEEMC764gkvFZvRU7Q';

                var map = L.mapbox.map('map','travelallrussia.a732a1d3',{
                    minZoom: 3,
                    maxZoom: 10,
                    scrollWheelZoom: false,
                }).setView(getMiddle(stations),6);

                for (var station in stations) {
                    drawMarker(stations[station]);
                }

                L.polyline([
                    L.latLng(stations['departure_station'].lat, stations['departure_station'].lng),
                    L.latLng(stations['arrival_station'].lat, stations['arrival_station'].lng),
                ], {
                    color: '#C10017',
                    weight: 3,
                    opacity: 1,
                    smoothFactor: 1
                }).addTo(map);
            }

            function drawMarker(station) {
                L.marker([station.lat, station.lng], {
                    icon: L.divIcon({
                        // Specify a class name we can refer to in CSS.
                        className: 'station-icon',
                        // Set marker width and height
                        iconSize: [15, 15],
                    })
                }).addTo(map);

                L.marker([station.lat,station.lng], {
                    icon: L.divIcon({
                        // Specify a class name we can refer to in CSS.
                        className: 'station-label',
                        html: '<span>' + station.name + '</span>',
                        iconSize: [100, 20]
                    })
                }).addTo(map);
            }

            function getMiddle(stations) {
                var lat = 0;
                var lng = 0;
                var count = 0;

                for (var station in stations) {
                    lat += parseFloat(stations[station].lat);
                    lng += parseFloat(stations[station].lng);
                    count++;
                }

                return [lat/count, lng/count];
            }

        }
    };

})(jQuery, Drupal, drupalSettings);
