<?php

namespace Drupal\store\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Store order entities.
 */
class StoreOrderViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['store_order']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Store order'),
      'help' => $this->t('The Store order ID.'),
    );

    return $data;
  }

}
