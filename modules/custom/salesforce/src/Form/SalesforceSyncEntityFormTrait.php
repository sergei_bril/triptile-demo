<?php

namespace Drupal\salesforce\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\salesforce\SalesforceSync;
use Drupal\salesforce\Entity\MappableEntityInterface;

trait SalesforceSyncEntityFormTrait {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['salesforce_sync'] = [
      '#type' => 'checkbox',
      '#default_value' => 1,
      '#title' => $this->t('Do synchronize with SalesForce?'),
      '#weight' => 14,
    ];

    return $form;
  }

  /**
   * Trigger Salesforce sync action 'PUSH'.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $entity
   */
  protected function salesforceBaseTrigger(FormStateInterface $form_state, $entity) {
    if ($form_state->getValue('salesforce_sync')) {
      if (($plugin_id = $this->mappingManager->findPluginForEntityType($entity->getEntityTypeId())) && $entity instanceof MappableEntityInterface) {
        $definition = $this->mappingManager->getDefinition($plugin_id);
        if (in_array(SalesforceSync::OPERATION_UPDATE, $definition['entity_operations'])) {
          $this->salesforceSync->setTriggerForEntity(SalesforceSync::SYNC_ACTION_PUSH, $entity->id(), $entity->getEntityTypeId());
        }
      }
    }
  }

}
