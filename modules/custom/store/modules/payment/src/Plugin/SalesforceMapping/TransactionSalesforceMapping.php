<?php

namespace Drupal\payment\Plugin\SalesforceMapping;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\payment\Entity\Transaction;
use Drupal\salesforce\Plugin\SalesforceMapping\SalesforceMappingBase;
use Drupal\salesforce\SalesforceException;
use Drupal\salesforce\SalesforceSync;

/**
 * Class TransactionSalesforceMapping
 *
 * @SalesforceMapping(
 *   id = "transaction_payment_transaction",
 *   label = @Translation("Mapping of Transaction to Payment_transaction__c"),
 *   entity_type_id = "transaction",
 *   salesforce_object = "Payment_transaction__c",
 *   entity_operations = {"update", "delete"},
 *   object_operations = {},
 *   priority = "drupal"
 * )
 */
class TransactionSalesforceMapping extends SalesforceMappingBase {

  const
    FIRSTPAYMENTS_RECORD_TYPE = 'FirstPayments',
    PAYPAL_RECORD_TYPE = 'Paypal',
    WALLET_ONE_RECORD_TYPE = 'WalletOne';

  /**
   * {@inheritdoc}
   * @param \Drupal\payment\Entity\Transaction $transaction
   */
  protected function doImport(EntityInterface $transaction, \stdClass $record) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   * @param \Drupal\payment\Entity\Transaction $transaction
   */
  protected function doExport(EntityInterface $transaction, \stdClass $record) {
    $invoiceMappingObject = $this->assureExport($transaction->getInvoice());
    if (!$invoiceMappingObject || !$invoiceMappingObject->getRecordId()) {
      throw new SalesforceException('Related invoice has not been exported yet.');
    }

    $dateChanged = new DrupalDateTime('now', new \DateTimeZone('UTC'));
    $dateChanged->setTimestamp($transaction->getChangedTime());
    $dateCreated = new DrupalDateTime('now', new \DateTimeZone('UTC'));
    $dateCreated->setTimestamp($transaction->getCreatedTime());

    $record->Name                 = 'RN-' . $transaction->id(); // @todo get site code from config
    $record->Invoice__c           = $invoiceMappingObject->getRecordId();
    $record->Status__c            = $transaction->getStatus();
    $record->CurrencyIsoCode      = $transaction->getAmount()->getCurrencyCode();
    $record->Amount__c            = $transaction->getAmount()->getNumber();
    $record->Currency_rate__c     = $transaction->getCurrencyRate();
    $record->Client_IP_address__c = $transaction->getIPAddress();
    $record->Merchant_ID__c       = $transaction->getMerchant()->getMerchantId();
    $record->Payment_by__c        = $this->getPaymentBy($transaction);
    $record->Transaction_type__c  = $this->getTransactionType($transaction);
    $record->Updated_datetime__c  = $dateChanged->format('c');
    $record->Created_datetime__c  = $dateCreated->format('c');

    if ($transaction->getStatus() == Transaction::STATUS_FAILED) {
      $record->Error_messages__c = $transaction->getMessage();
    }

    switch ($transaction->getMerchant()->getPaymentAdapter()) {
      case 'paypal_ec':
      case 'paypal_wpp':
        $record->Paypal_transaction_ID__c = $transaction->getRemoteId();
    }

    if (!$this->mappingObject->getRecordId()) {
      if (!$id = $this->getRecordTypeId($transaction)) {
        throw new SalesforceException('Can\'t get appropriate record type id for transaction.');
      }
      $record->RecordTypeId = $id;
    }

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryConditions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isSyncAllowed($action) {
    /** @var \Drupal\payment\Entity\Transaction $transaction */
    $transaction = $this->getEntity();
    return ($action == SalesforceSync::SYNC_ACTION_DELETE || $transaction->getInvoice()) && $transaction->getMerchant()->getPaymentAdapter() != 'simple';
  }

  /**
   * Gets appropriate record type for the transaction being exported.
   *
   * @param \Drupal\payment\Entity\Transaction $transaction
   * @return string|null
   */
  protected function getRecordTypeId(Transaction $transaction) {
    $recordTypes = $this->getRecordTypes();
    $id = null;
    switch ($transaction->getMerchant()->getPaymentAdapter()) {
      case 'paypal_ec':
      case 'paypal_wpp':
      case 'simple':
        $id = array_search(static::PAYPAL_RECORD_TYPE, $recordTypes);
    }

    return $id;
  }

  /**
   * Gets payment by string.
   *
   * @param \Drupal\payment\Entity\Transaction $transaction
   * @return string
   */
  protected function getPaymentBy(Transaction $transaction) {
    switch ($transaction->getPaymentMethod()) {
      case 'credit_card':
        return 'Credit card';
      case 'paypal':
        return 'PayPal';
    }
  }

  /**
   * Gets transaction type name.
   *
   * @param \Drupal\payment\Entity\Transaction $transaction
   * @return string
   */
  protected function getTransactionType(Transaction $transaction) {
    switch ($transaction->getType()) {
      case Transaction::TYPE_PAYMENT:
        return 'payment';
      case Transaction::TYPE_REFUND:
        return 'refund';
    }
  }

}