<?php

namespace Drupal\trip_base\Plugin\SalesforceMapping;

use Drupal\Core\Entity\EntityInterface;
use Drupal\salesforce\Plugin\SalesforceMapping\SalesforceMappingBase;
use Drupal\salesforce\SalesforceException;

/**
 * Class ConnectionSalesforceMapping
 *
 * @SalesforceMapping(
 *   id = "connection_connection",
 *   label = @Translation("Mapping of Connection to Connection__c"),
 *   entity_type_id = "connection",
 *   salesforce_object = "Connection__c",
 *   entity_operations = {},
 *   object_operations = {"update", "delete"},
 *   priority = "salesforce"
 * )
 */
class ConnectionSalesforceMapping extends SalesforceMappingBase {

  public function getImportFields() {
    $fields = [
      'Name',
      'A__c',
      'B__c',
      'Type__c',
      'Rating__c',
      'Overall_rating__c'
    ];

    return array_merge(parent::getImportFields(), $fields);
  }

  /**
   * {@inheritdoc}
   * @param \Drupal\trip_base\Entity\Connection $connection
   */
  protected function doImport(EntityInterface $connection, \stdClass $record) {
    $hubAMappingObject = $this->assureImport($record->A__c, 'Hub__c');
    if (!$hubAMappingObject || !$hubAMappingObject->getMappedEntityId()) {
      throw new SalesforceException('Point A has not been imported yet');
    }
    // todo set data
  }

  /**
   * {@inheritdoc}
   * @param \Drupal\trip_base\Entity\Connection $connection
   */
  protected function doExport(EntityInterface $connection, \stdClass $record) {
    // Do nothing
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryConditions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isSyncAllowed($action) {
    return true;
  }

}
