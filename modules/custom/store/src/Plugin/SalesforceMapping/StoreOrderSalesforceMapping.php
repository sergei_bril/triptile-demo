<?php

namespace Drupal\store\Plugin\SalesforceMapping;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\salesforce\Annotation\SalesforceMapping;
use Drupal\salesforce\Plugin\SalesforceMapping\SalesforceMappingBase;
use Drupal\salesforce\SalesforceException;
use Drupal\salesforce\SalesforceSync;
use Drupal\salesforce\SelectQuery;
use Drupal\store\Entity\StoreOrder;
use Drupal\train_base\Entity\TrainTicket;

/**
 * Class StoreOrderSalesforceMapping
 *
 * @SalesforceMapping(
 *   id = "store_order_opportunity",
 *   label = @Translation("Mapping of Store order to Opportunity"),
 *   entity_type_id = "store_order",
 *   salesforce_object = "Opportunity",
 *   entity_operations = {"update", "delete"},
 *   object_operations = {"update", "delete"},
 *   priority = "drupal"
 * )
 */
class StoreOrderSalesforceMapping extends SalesforceMappingBase {

  const TRAIN_TICKET_RECORD_TYPE = 'Train ticket';

  const ORDER2_RAIL_TICKET_RECORD_TYPE = 'Rail / bus ticket';

  const
    ORDER2_LOGIST_EMAIL = 'reservations@firebirdtours.com',
    ORDER2_ACCOUNT_EMAIL = 'reservations@firebirdtours.com';

  const
    ORDER2_E_TICKET_TICKET_TYPE = 'E-ticket',
    ORDER2_PAPER_TICKET_TICKET_TYPE = 'Paper ticket',
    ORDER2_BOARDING_PASS_TICKET_TYPE = 'Boarding pass';

  /**
   * {@inheritdoc}
   */
  public function export() {
    $opportunity = parent::export();

    if (!$this->getEntity()->getData('Order2 exported')) {
      try {
        $this->createOrder2($opportunity);
        $order = $this->getEntity();
        $order->setData('Order2 exported', true)->save();
      }
      catch (\Exception $exception) {
        // Since oppty already created but it's id is not on mapping object we
        // try to suppress any exceptions to avoid from leaving oppty not mapped.
        watchdog_exception('salesforce', $exception);
      }
    }

    return $opportunity;
  }

  /**
   * {@inheritdoc}
   */
  public function getImportFields() {
    $fields = [
      'AccountId',
      'StageName',
      'Train_departure__c',
    ];

    // @todo Fields that were in cluster. Do we need to import them?
    // Site_link__c - order visibility.

    return array_merge(parent::getImportFields(), $fields);
  }

  /**
   * {@inheritdoc}
   * @param \Drupal\store\Entity\StoreOrder $order
   */
  protected function doImport(EntityInterface $order, \stdClass $record) {
    $userMappingObject = $this->assureImport($record->AccountId, 'Account');
    if (!$userMappingObject || !$userMappingObject->getMappedEntityId()) {
      throw new SalesforceException('Order owner has not been imported yet.');
    }

    $order->setOwnerId($userMappingObject->getMappedEntityId());
    if ($status = $this->salesforceToLocalOrderStatus($record->StageName)) {
      $order->setStatus($status);
    }
    if ($record->Train_departure__c) {
      $order->setTicketIssueDate(new DrupalDateTime($record->Train_departure__c));
    }
  }

  /**
   * {@inheritdoc}
   * @param \Drupal\store\Entity\StoreOrder $order
   */
  protected function doExport(EntityInterface $order, \stdClass $record) {
    $user_mapping_object = $this->assureExport($order->getOwner());
    if (!$user_mapping_object || !$user_mapping_object->getRecordId()) {
      throw new SalesforceException('Order owner has not been exported yet.');
    }

    // Base info.
    $close_date = new DrupalDateTime(date('c', $order->getCreatedTime()), new \DateTimeZone('UTC'));
    $record->ExtId__c           = $order->getHash();
    $record->AccountId          = $user_mapping_object->getRecordId();
    $record->Site__c            = $order->getSiteCode();
    $record->Validation_pass__c = true;
    $record->CurrencyIsoCode    = $order->getOrderTotal()->getCurrencyCode();
    $record->Amount             = $order->getOrderTotal()->getNumber();
    $record->Name               = $this->opportunityName($order);
    $record->CloseDate          = $close_date->format('c');
    $record->Language__c        = $order->language()->getId();
    $record->order_reference__c = $order->getOrderNumber();
    $record->order_status__c    = $order->getStatus();
    $record->Site_request__c    = true;
    $record->is_eticket__c      = true;
    $record->StageName          = $this->localToSalesforceOrderStatus($order->getStatus());
    $record->Email__c           = $order->getOwner()->getEmail();
    $record->Description        = $order->getNotes();

    if (!empty($order->getTicketIssueDate())) {
      $record->Train_departure__c = $order->getTicketIssueDate()->format(DATETIME_DATE_STORAGE_FORMAT);
    }

    if (!empty($order->getNotes())) {
      $record->Description      = $order->getNotes();
    }

    if (!$this->mappingObject->getRecordId()) {
      $record->RecordTypeId     = $this->getRecordTypeId($order);
    }

    $record = $this->exportCustomerInfo($order, $record);
    $record = $this->exportTicketsInfo($order, $record);

    // @todo Related logic hasn't been implemented yet.
    // $record->Cancel_request__c = $order->cancelRequested();
    // $record->Device__c = $order->getDevice();
    // $record->Device_OS__c = $order->getDeviceOs();
    // $record->Screen_res__c = $order->getScreenResolution();
    // $record->Transaction_descriptor__c = $transaction->getDescriptor(); // soft descriptor
    // $record->Save_search__c = $order->getSaveSearch();
    // $record->E_ticket_booking_number__c = $order->getEticketBookingNumber();
    // $record->Details_received__c = $optional_service_details;
    // $record->Preferred_seats__c
    // $record->Modification_request__c
    // $record->supplier_reference__c
    // $record->supplier_code__c

    // @todo Do we still need this fields?
    // $record->payment_status__c = $order->getPaymentStatus();
    // Delivery
    // $record->Delivery_information__c
    // $record->Delivery_status__c = 'Requested';
    // $record->order_error__c

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryConditions() {
    $conditions = [];
    $conditions[] = [
      'field' => 'Site__c',
      'value' => "'RN'", // @todo Replace with value from config.
      'operator' => '=',
    ];
    $id = array_search(static::TRAIN_TICKET_RECORD_TYPE, $this->getRecordTypes());
    $conditions[] = [
      'field' => 'RecordTypeId',
      'value' => "'" . $id . "'",
      'operator' => '=',
    ];
    // $conditions[] = [
    //   'field' => 'Site_publish__c',
    //   'value' => 'FALSE',
    //   'operator' => '=',
    // ];

    return $conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function isSyncAllowed($action) {
    if (!$this->mappingObject) {
      throw new SalesforceException('Salesforce mapping object is not set.');
    }

    return $action == SalesforceSync::SYNC_ACTION_DELETE || $this->mappingObject->getMappedEntityId();
  }

  /**
   * {@inheritdoc}
   */
  protected function createEntity() {
    return StoreOrder::create([
      'type' => 'train_order'
    ]);
  }

  /**
   * Generates opportunity name.
   *
   * @param \Drupal\store\Entity\StoreOrder $order
   * @return string
   */
  protected function opportunityName(StoreOrder $order) {
    return 'Ticket order ' . $order->getOrderNumber();
  }

  /**
   * Gets appropriate opportunity record type id.
   *
   * @param \Drupal\store\Entity\StoreOrder $order
   * @return string
   */
  protected function getRecordTypeId(StoreOrder $order) {
    return array_search(static::TRAIN_TICKET_RECORD_TYPE, $this->getRecordTypes());
  }

  /**
   * Adds customer info to the opportunity.
   *
   * @param \Drupal\store\Entity\StoreOrder $order
   * @param \stdClass $record
   * @return \stdClass
   */
  protected function exportCustomerInfo(StoreOrder $order, \stdClass $record) {
    $customer_profile = null;
    $address = null;
    $card_details = null;
    $transaction = null;
    $geoip_data = null;
    /** @var \Drupal\store\Entity\Invoice $invoice */
    foreach ($order->getInvoices() as $invoice) {
      if ($invoice->isPaid()) {
        // Below condition is set for case with Simple merchant when customer profile may not exist
        if ($customer_profile = $invoice->getCustomerProfile()) {
          $address = $customer_profile->getAddress();
          /** @var \Drupal\payment\Entity\Transaction $invoice_transaction */
          foreach ($invoice->getTransactions() as $invoice_transaction) {
            if ($invoice_transaction->isSuccess()) {
              $transaction = $invoice_transaction;
              $card_details = sprintf("Transaction: %s\n%s %s\n%s=%s=%s=\n%s\n%s\n%s %s %s\n%s\n%s\n",
                $transaction->id(), $transaction->getAmount()->getNumber(),
                $transaction->getAmount()->getCurrencyCode(), $address->getGivenName(),
                $address->getAdditionalName(), $address->getFamilyName(), $customer_profile->getPhoneNumber(),
                $address->getAddressLine1(), $address->getLocality(), $address->getAdministrativeArea(),
                $address->getPostalCode(), $address->getCountryCode(), $customer_profile->getEmail()
              );
              if (function_exists('geoip_record_by_name')) {
                $geoip_data = @geoip_record_by_name($transaction->getIPAddress());
              }

              break;
            }
          }
        }

        break;
      }
    }

    if ($customer_profile) {
      $record->CC_country__c    = $address->getCountryCode();
    }

    if ($card_details) {
      $record->card_details__c  = substr($card_details, 0, 255);
    }

    if ($transaction) {
      $record->ip_address__c    = $transaction->getIPAddress();
      if ($geoip_data) {
        $record->ip_country__c  = $geoip_data['country_code'];
        $record->ip_city__c     = $geoip_data['city'];
        $record->ip_info__c     = '';
        foreach ($geoip_data as $key => $value) {
          $record->ip_info__c   .= sprintf("%s: %s\n", $key, $value);
        }
      }
    }

    return $record;
  }

  /**
   * Adds ticket info to the opportunity.
   *
   * @param \Drupal\store\Entity\StoreOrder $order
   * @param \stdClass $record
   * @return \stdClass
   */
  protected function exportTicketsInfo(StoreOrder $order, \stdClass $record) {
    $boarding_pass = false;
    $boarding_pass_instructions = [];
    $start_date = $end_date = 0;
    $forwardCarNumbers = $returnCarNumbers = [];
    $depStation1 = $depStation2 = $arrStation1 = $arrStation2 = null;
    $depAddress1 = $depAddress2 = $arrAddress1 = $arrAddress2 = null;
    $chnStation1 = $chnStation2 = null;
    $ticketsInfo = [];
    $travelers = [];
    $pax = 0;
    $passengerCitizenships = [];
    /** @var \Drupal\train_base\Entity\TrainTicket $ticket */
    foreach ($order->getTickets() as $ticket) {
      $leg = $ticket->getLegNumber();
      $ticketDepStation = $ticket->getDepartureStation();
      $ticketArrStation = $ticket->getArrivalStation();
      $ticketChnStation = $ticket->getChangeStation();

      $boarding_pass = $boarding_pass || $ticket->isBoardingPassRequired();
      if ($ticket->isBoardingPassRequired()) {
        if ($ticketChnStation) {
          $instruction = sprintf('#%s %s--%s--%s', $ticket->getTrainNumber(), $ticketDepStation, $ticketChnStation, $ticketArrStation);
        }
        else {
          $instruction = sprintf('#%s %s--%s', $ticket->getTrainNumber(), $ticketDepStation, $ticketArrStation);
        }
        if (end($boarding_pass_instructions) != $instruction) {
          $boarding_pass_instructions[] = $instruction;
        }
      }

      // Train numbers.
      if ($leg == 1) {
        $record->Train_1__c = $ticket->getTrainNumber();
      }
      elseif ($leg == 2) {
        $record->Train_2__c = $ticket->getTrainNumber();
      }

      // Start and End dates.
      if (!$start_date || ($ticket->getDepartureDateTime() && $start_date > $ticket->getDepartureDateTime()->getTimestamp())) {
        $start_date = $ticket->getDepartureDateTime()->getTimestamp();
      }
      if (!$end_date || ($ticket->getArrivalDateTime() && $end_date < $ticket->getArrivalDateTime()->getTimestamp())) {
        $end_date = $ticket->getArrivalDateTime()->getTimestamp();
      }

      // Car numbers
      if ($leg == 2) {
        $returnCarNumbers[$ticket->getCoachNumber()] = $ticket->getCoachNumber();
      }
      else {
        $forwardCarNumbers[$ticket->getCoachNumber()] = $ticket->getCoachNumber();
      }

      // Departure stations.
      if ($leg == 1) {
        $depStation1 = $ticketDepStation;
        if ($address = $depStation1->getAddress()) {
          $depAddress1 = sprintf('%s, %s, %s', $address->getAddressLine1(), $address->getLocality(),
            \Drupal::service('address.country_repository')->get($address->getCountryCode()));
        }
      }
      elseif ($leg == 2) {
        $depStation2 = $ticketDepStation;
        if ($address = $depStation2->getAddress()) {
          $depAddress2 = sprintf('%s, %s, %s', $address->getAddressLine1(), $address->getLocality(),
            \Drupal::service('address.country_repository')->get($address->getCountryCode()));
        }
      }

      // Arrival stations.
      if ($leg == 1) {
        $arrStation1 = $ticketArrStation;
        if ($address = $arrStation1->getAddress()) {
          $arrAddress1 = sprintf('%s, %s, %s', $address->getAddressLine1(), $address->getLocality(),
            \Drupal::service('address.country_repository')->get($address->getCountryCode()));
        }
      }
      elseif ($leg == 2) {
        $arrStation2 = $ticketArrStation;
        if ($address = $arrStation2->getAddress()) {
          $arrAddress2 = sprintf('%s, %s, %s', $address->getAddressLine1(), $address->getLocality(),
            \Drupal::service('address.country_repository')->get($address->getCountryCode()));
        }
      }

      // Change station
      if ($leg == 1) {
        $chnStation1 = $ticketChnStation;
      }
      elseif ($leg == 2) {
        $chnStation2 = $ticketChnStation;
      }

      // Ticket info
      $key = $ticketDepStation->id() . $ticketArrStation->id() . $ticket->getTrainNumber()
        . $ticket->getCoachClass()->id() . $ticket->getSeatType()->id();
      if (!isset($ticketsInfo[$key])) {
        $ticketsInfo[$key] = [
          'count' => 1,
          'text' => $this->generateTicketsInfo($ticket),
        ];
      }
      else {
        $ticketsInfo[$key]['count']++;
      }

      // Passenger info.
      /** @var \Drupal\train_base\Entity\Passenger $passenger */
      foreach ($ticket->getPassengers() as $passenger) {
        $passengerText = $passenger->getFirstName() . '=' . $passenger->getLastName();

        $dob = $passenger->getDob() ? $passenger->getDob()->format('Y-m-d') : '';
        $details = array_filter([$passenger->getGender(), $passenger->getIdNumber(), $dob, $passenger->getCitizenship()]);
        if (!empty($details)) {
          $passengerText .= ' (' . implode(', ', $details) . ')';
        }
        $travelers[] = $passengerText;

        if ($passenger->getCitizenship()) {
          $passengerCitizenships[] = $passenger->getCitizenship();
        }
      }
      // @todo Use pax from ticket when it is added.
      $pax++;

    }


    $record->board_pass__c = $boarding_pass;
    if (!empty($boarding_pass_instructions)) {
      $record->Boarding_pass_instructions__c  = substr(implode(', ', $boarding_pass_instructions), 0, 255);
    }
    if ($start_date) {
      $record->Tour_start_date__c             = date('c', $start_date);
    }
    if ($end_date) {
      $record->Tour_end_date__c               = date('c', $end_date);
    }
    if (!empty($forwardCarNumbers)) {
      $record->Car__c                         = implode(',', $forwardCarNumbers);
    }
    if (!empty($returnCarNumbers)) {
      $record->Return_car__c                  = implode(',', $returnCarNumbers);
    }
    if ($depStation1) {
      $record->Departure_station__c           = $depStation1->getParentStation() ? $depStation1->getName() : 'Central station';
      $record->Address__c                     = $depAddress1;
    }
    if ($depStation2) {
      $record->Departure_station_2__c         = $depStation2->getParentStation() ? $depStation2->getName() : 'Central station';
      $record->Address_2__c                   = $depAddress2;
    }
    if ($arrStation1) {
      $record->Arrival_station__c             = $arrStation1->getParentStation() ? $arrStation1->getName() : 'Central station';
      $record->Address_arrival__c             = $arrAddress1;
    }
    if ($arrStation2) {
      $record->Arrival_station_2__c           = $arrStation2->getParentStation() ? $arrStation2->getName() : 'Central station';
      $record->Address_arrival_2__c           = $arrAddress2;
    }
    if ($chnStation1) {
      $record->Change_station__c              = $chnStation1->getParentStation() ? $chnStation1->getName() : 'Central station';
    }
    if ($chnStation2) {
      $record->Change_station_2__c            = $chnStation2->getParentStation() ? $chnStation2->getName() : 'Central station';
    }
    if (!empty($ticketsInfo)) {
      $record->RT_ticket_info__c              = '';
      foreach ($ticketsInfo as $item) {
        $record->RT_ticket_info__c            .= str_replace('%COUNT%', $item['count'], $item['text']);
      }
    }
    if (!empty($travelers)) {
      $record->Traveler_names__c              = implode("\n", $travelers);
      $record->Is_direct__c                   = false;
      $record->Nationality__c                 = implode(",", $passengerCitizenships);
    }
    else {
      $record->Is_direct__c                   = true;
    }
    $record->PAX__c                           = $pax;

    return $record;
  }

  /**
   * Converts Opportunity stage to store order status.
   *
   * @param string $stage
   * @return bool|string
   */
  protected function salesforceToLocalOrderStatus($stage) {
    switch ($stage) {
      case 'Modification request':
        return StoreOrder::STATUS_MODIFICATION_REQUESTED;

      case 'Modified':
        return StoreOrder::STATUS_MODIFIED;

      case 'Verification':
      case 'Processing':
        return StoreOrder::STATUS_PROCESSING;

      case 'Refund requested':
        return StoreOrder::STATUS_CANCELLATION_REQUESTED;

      case 'Booked':
        return StoreOrder::STATUS_BOOKED;

      case 'Canceled':
        return StoreOrder::STATUS_CANCELED;

      default:
        return false;
    }
  }

  /**
   * Converts Opportunity stage to store order status.
   *
   * @param string $status
   * @return bool|string
   */
  protected function localToSalesforceOrderStatus($status) {
    switch ($status) {
      case StoreOrder::STATUS_MODIFICATION_REQUESTED:
        return 'Modification request';

      case StoreOrder::STATUS_MODIFIED:
        return 'Modified';

      case StoreOrder::STATUS_CANCELLATION_REQUESTED:
        return 'Refund requested';

      case StoreOrder::STATUS_BOOKED:
        return 'Booked';

      case StoreOrder::STATUS_PROCESSING:
        return 'Processing';

      case StoreOrder::STATUS_CANCELED:
        return 'Canceled';

      default:
        return 'Processing';
    }
  }

  /**
   * Generates the text for field RT_ticket_info__c.
   *
   * @param \Drupal\train_base\Entity\TrainTicket $ticket
   * @return string
   */
  protected function generateTicketsInfo(TrainTicket $ticket) {
    $services = [];
    /** @var \Drupal\train_base\Entity\CarService $carService */
    foreach ($ticket->getCoachClass()->getCarServices() as $carService) {
      $services[] = $carService->getName();
    }
    $depDateTime = $ticket->getDepartureDateTime();
    $depDateTime->setTimezone($ticket->getDepartureStation()->getTimezone());
    $depCity = $ticket->getDepartureStation()->getParentStation() ? : $ticket->getDepartureStation();
    $arrCity = $ticket->getArrivalStation()->getParentStation() ? : $ticket->getArrivalStation();
    /** @var \Drupal\store\Entity\StoreOrder $order */
    $order = $this->getEntity();
    $orderItems = $order->getOrderItems();
    /** @var \Drupal\store\Entity\OrderItem $orderItem */
    foreach ($orderItems as $orderItem) {
       if ($orderItem->bundle() == 'ticket' && $orderItem->getLegNumber() == $ticket->getLegNumber() && $orderItem->getProduct()) {
         $product = $orderItem->getProduct();
         $product_description = $product->getDescription();
         break;
       }
    }

    $ticketText = [];
    $ticketText['train'] = 'Train #' . $ticket->getTrainNumber() . ' '
      . $depCity . ' (' . $ticket->getDepartureStation()->getAddress()->getCountryCode() . ')';
    if ($ticket->getChangeStation()) {
      $chnCity = $ticket->getChangeStation()->getParentStation() ? : $ticket->getChangeStation();
      $ticketText['train'] .= '--' . $chnCity . ' (' . $ticket->getChangeStation()->getAddress()->getCountryCode() . ')';
    }
    $ticketText['train'] .= '--' . $arrCity . ' (' . $ticket->getArrivalStation()->getAddress()->getCountryCode() . ').';

    $ticketText['datetime'] = 'Departure date/time: ' . $depDateTime->format('D, j M Y, H:i') . '.';

    $ticketText['ticket'] = 'Ticket class: ' . $ticket->getCoachClass() . '. '
      . $ticket->getSeatType() . '.';
    if (!empty($services)) {
      $ticketText['ticket'] .= ' ' . implode(', ', $services) . '.';
    }
    $ticketText['count'] = 'Number of tickets %COUNT%.';
    if (!empty($product_description)) {
      $ticketText['product_description'] = $product_description;
    }

    return implode("\n", $ticketText) . "\n";
  }

  /**
   * Creates a new Order2 record in Salesforce.
   *
   * @param \stdClass $opportunity
   */
  protected function createOrder2(\stdClass $opportunity) {
    $masterOrder2 = new \stdClass();
    $masterOrder2->RecordTypeId = array_search(static::ORDER2_RAIL_TICKET_RECORD_TYPE, $this->getRecordTypes('Order2__c'));
    $masterOrder2->Opportunity__c = $opportunity->Id;
    $masterOrder2->Account__c = $this->getOrder2AccountId();
    $masterOrder2->Logist__c = $this->getOrder2LogistId();

    $prevDepStation = null;
    $orders = $ticketsInfo = [];
    /** @var \Drupal\store\Entity\StoreOrder $storeOrder */
    $storeOrder = $this->getEntity();
    $orderItems = $storeOrder->getOrderItems();
    /** @var \Drupal\train_base\Entity\TrainTicket $ticket */
    foreach ($storeOrder->getTickets() as $ticket) {
      $leg = $ticket->getLegNumber();
      $depStation = $ticket->getDepartureStation();
      $arrStation = $ticket->getArrivalStation();

      // Ticket info
      if (!isset($ticketsInfo[$leg])) {
        $ticketsInfo[$leg] = [
          'count' => 1,
          'text' => $this->generateTicketsInfo($ticket),
        ];
      }
      else {
        $ticketsInfo[$leg]['count']++;
      }

      if (!isset($orders[$leg])) {
        $order2 = clone $masterOrder2;
        $order2->Start_date__c = date('c', $ticket->getDepartureDateTime()->getTimestamp());
        $order2->End_date__c = date('c', $ticket->getDepartureDateTime()->getTimestamp());
        $order2->Location__c = (string) ($depStation->getParentStation() ? $depStation->getParentStation() : $depStation);
        $order2->Destination__c = (string) ($arrStation->getParentStation() ? $arrStation->getParentStation() : $arrStation);
        $order2->Time__c = $ticket->getDepartureDateTime()->format('H:i');
        $order2->Type__c = $ticket->isBoardingPassRequired() ? static::ORDER2_BOARDING_PASS_TICKET_TYPE : static::ORDER2_E_TICKET_TICKET_TYPE;

        /** @var \Drupal\store\Entity\OrderItem $orderItem */
        foreach ($orderItems as $orderItem) {
          if ($orderItem->bundle() == 'ticket' && $orderItem->getProduct()) {
            $product = $orderItem->getProduct();
            $order2->Cost__c = $product->getPrice()->getNumber() * $orderItem->getQuantity();
            $order2->CurrencyIsoCode = $product->getPrice()->getCurrencyCode();
            break;
          }
        }

        $orders[$leg] = $order2;
      }
    }

    foreach ($orders as $leg => $order2) {
      $order2->Order_details__c = str_replace('%COUNT%', $ticketsInfo[$leg]['count'], $ticketsInfo[$leg]['text']);
      $this->salesforceApi->createRecord('Order2__c', $order2);
    }
  }

  /**
   * Gets the default logist assigned to Order2.
   *
   * @return string
   */
  protected function getOrder2LogistId() {
    $id = $this->cacheBackend->get('order2_default_logist_id');
    if (!$id) {
      $query = new SelectQuery('Logist__c');
      $query->condition('Email__c', "'" . static::ORDER2_LOGIST_EMAIL . "'")
        ->field('Id');
      $logists = $this->salesforceApi->query($query);
      $logist = reset($logists);
      if ($logist) {
        $id = $logist->Id;
      }
      $this->cacheBackend->set('order2_default_logist_id', $id);
    }
    else {
      $id = $id->data;
    }

    return $id;
  }

  /**
   * Gets the default account the Order2 is assigned to.
   *
   * @return string
   */
  protected function getOrder2AccountId() {
    $id = $this->cacheBackend->get('order2_default_account_id');
    if (!$id) {
      $query = new SelectQuery('Account');
      $query->condition('Custom_email__c', "'" . static::ORDER2_ACCOUNT_EMAIL . "'")
        ->field('Id');
      $accounts = $this->salesforceApi->query($query);
      $account = reset($accounts);
      if ($account) {
        $id = $account->Id;
      }
      $this->cacheBackend->set('order2_default_account_id', $id);
    }
    else {
      $id = $id->data;
    }

    return $id;
  }

}
