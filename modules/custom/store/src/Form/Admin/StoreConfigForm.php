<?php

namespace Drupal\store\Form\Admin;

use Drupal\master\Form\Admin\ConfigForm;
use Drupal\Core\Form\FormStateInterface;

class StoreConfigForm extends ConfigForm {

  /**
   * Mapping of country code to currency.
   *
   * @var array
   */
  protected static $countryCurrencyMapping = [
    'US' => 'USD',
    'GB' => 'GBP',
    'DE' => 'EUR',
    'RU' => 'USD',
    'AT' => 'EUR',
    'BE' => 'EUR',
    'GR' => 'EUR',
    'IE' => 'EUR',
    'ES' => 'EUR',
    'IT' => 'EUR',
    'LV' => 'EUR',
    'LT' => 'EUR',
    'LU' => 'EUR',
    'NL' => 'EUR',
    'PT' => 'EUR',
    'SK' => 'EUR',
    'SI' => 'EUR',
    'FI' => 'EUR',
    'FR' => 'EUR',
    'EE' => 'EUR',
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'store_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get currency options (available).
    $currency_options = \Drupal::service('currency.form_helper')->getCurrencyOptions();
    // @todo will delete on production.
    unset($currency_options['XXX']);

    $config = $this->configFactory->get('store.settings');

    // Global site currency.
    $global_currency = $config->get('global_currency');
    $form['global_currency_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Global site currency'),
    ];
    $form['global_currency_fieldset']['global_currency'] = [
      '#title' => $this->t('Global currency'),
      '#type' => 'select',
      '#options' => $currency_options,
      '#default_value' => !empty($global_currency) ? $global_currency : null,
      '#empty_option' => t('- None -'),
    ];

    // Country currency.
    $country_currency = $config->get('country_currency');
    if (empty($country_currency)) {
      foreach (static::$countryCurrencyMapping as $country_code => $currency_code) {
        $country_currency[] = ['country_code' => $country_code, 'currency_code' => $currency_code];
      }
    }
    $name_field = $form_state->get('num_countries');
    $form['#tree'] = TRUE;
    $form['country_currency_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Country currency'),
      '#prefix' => '<div id="country-currency-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    if (!isset($name_field)) {
      $name_field = count($country_currency);
      $form_state->set('num_countries', $name_field);
    }
    $form['country_currency_fieldset']['country_currency'] = [
      '#type' => 'container',
    ];
    for ($i = 0; $i < $name_field; $i++) {
      $form['country_currency_fieldset']['country_currency'][$i] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'container-inline'
          ],
        ],
      ];
      $form['country_currency_fieldset']['country_currency'][$i]['country_code'] = [
        '#type' => 'select',
        //'#title' => t('Country', [], ['context' => 'Store config Form']),
        '#options' => $this->countryManager->getList(),
        '#default_value' => $country_currency[$i]['country_code'],
        '#empty_option' => t('- None -'),
      ];
      $form['country_currency_fieldset']['country_currency'][$i]['currency_code'] = [
        '#type' => 'select',
        //'#title' => t('Currency', [], ['context' => 'Store config Form']),
        '#options' => $currency_options,
        '#default_value' => $country_currency[$i]['currency_code'],
        '#empty_option' => t('- None -'),
      ];
    }
    $form['country_currency_fieldset']['actions']['add_country'] = [
      '#type' => 'submit',
      '#value' => t('Add one more'),
      '#submit' => array('::addOne'),
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'country-currency-fieldset-wrapper',
      ],
    ];
    if ($name_field > 0) {
      $form['country_currency_fieldset']['actions']['remove_country'] = [
        '#type' => 'submit',
        '#value' => t('Remove one'),
        '#submit' => array('::removeCallback'),
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'country-currency-fieldset-wrapper',
        ],
      ];
    }
    $form_state->setCached(FALSE);

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the stations in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $country_currency_field = $form_state->get('num_countries');
    return $form['country_currency_fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $country_currency_field = $form_state->get('num_countries');
    $add_button = $country_currency_field + 1;
    $form_state->set('num_countries', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $country_currency_field = $form_state->get('num_countries');
    if ($country_currency_field > 0) {
      $remove_button = $country_currency_field - 1;
      $form_state->set('num_countries', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('store.settings');

    // Global site currency.
    $global_currency = $form_state->getValue($form['global_currency_fieldset']['global_currency']['#parents']);
    $config->set('global_currency', $global_currency);

    // Country currency.
    $country_currency = $form_state->getValue($form['country_currency_fieldset']['country_currency']['#parents']);
    if (!empty($country_currency)) {
      $config->set('country_currency', $country_currency);
    }

    $config->save();
  }
}