<?php

namespace Drupal\store;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\store\Price;
use Drupal\store\PriceFactory;


class PriceRule {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * The PriceFactory.
   *
   * @var \Drupal\store\PriceFactory;
   */
  protected $priceFactory;

  /**
   * Price rule constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query factory.
   *
   * @param \Drupal\store\PriceFactory $price_factory
   */
  public function __construct(EntityTypeManager $entity_type_manager, QueryFactory $query_factory, PriceFactory $price_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queryFactory = $query_factory;
    $this->priceFactory = $price_factory;
  }

  public function updatePrice(string $type, Price $price, $data) {
    $applied_rules = [];
    if (isset($type) && isset($price)) {
      extract($data);
      $query = $this->queryFactory->get('price_rule');
      $query->condition('price_rule_type', $type);
      $query->sort('weight', 'ASC');
      $entity_ids = $query->execute();
      if (!empty($entity_ids)) {
        $entities = $this->entityTypeManager->getStorage('price_rule')
          ->loadMultiple($entity_ids);
        foreach ($entities as $entity) {
          /** @var \Drupal\store\Price $price
          *   @var \Drupal\store\Entity\PriceRule $entity **/
          $condition = $entity->getCondition();
          $tax_type = $entity->getTaxType();
          $tax_value = $entity->getTaxValue();
          $tax_value_currency = $entity->getTaxValueCurrency();
          if (eval('return ' . $condition . ';')) {
            switch ($tax_type) {
              case 'fixed':
                $tax_price = $this->priceFactory->get((string)$tax_value, $tax_value_currency);
                $price = $price->add($tax_price);
                $applied_rules[] = $entity->id();
                break;
              case 'rate':
                $price = $price->multiply($tax_value);
                $applied_rules[] = $entity->id();
                break;
            }
          }
        }
      }
    }

    return ['price' => $price, 'applied_rules' => $applied_rules];
  }

}