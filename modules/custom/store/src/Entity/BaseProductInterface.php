<?php

namespace Drupal\store\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\store\Price;

/**
 * Provides an interface for defining Base product entities.
 *
 * @ingroup store
 */
interface BaseProductInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Base product type.
   *
   * @return string
   *   The Base product type.
   */
  public function getType();

  /**
   * Gets the Base product name.
   *
   * @return string
   *   Name of the Base product.
   */
  public function getName();

  /**
   * Sets the Base product name.
   *
   * @param string $name
   *   The Base product name.
   *
   * @return \Drupal\store\Entity\BaseProductInterface
   *   The called Base product entity.
   */
  public function setName($name);

  /**
   * Gets the field form name for the service.
   *
   * @return string
   */
  public function getFieldForm();

  /**
   * Gets the Base product creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Base product.
   */
  public function getCreatedTime();

  /**
   * Sets the Base product creation timestamp.
   *
   * @param int $timestamp
   *   The Base product creation timestamp.
   *
   * @return \Drupal\store\Entity\BaseProductInterface
   *   The called Base product entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets supplier status.
   *
   * @return bool
   */
  public function isEnabled();

  /**
   * Gets the price as an object of type \Drupal\store\Price.
   *
   * @return \Drupal\store\Price
   */
  public function getPrice();

  /**
   * Sets product price value.
   *
   * @param float $price
   * @return static
   */
  public function setPrice($price);

  /**
   * Gets product price currency.
   *
   * @return string
   */
  public function getCurrency();

  /**
   * Gets product description.
   *
   * @return string
   */
  public function getDescription();

  /**
   * Gets the product weight.
   *
   * @return int
   */
  public function getWeight();

  /**
   * Checks if product is default.
   * If field 'default' doesn't exist it returns null.
   *
   * @return bool|null
   */
  public function isDefault();

  /**
   * Gets date when the product becomes available.
   * If field 'available_from' doesn't exist it returns null.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   */
  public function getAvailableFrom();

  /**
   * Gets date when the product becomes unavailable.
   * If field 'available_until' doesn't exist it returns null.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   */
  public function getAvailableUntil();

}
