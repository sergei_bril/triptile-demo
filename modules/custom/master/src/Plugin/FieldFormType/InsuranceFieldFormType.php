<?php

/**
 * @file
 * Contains \Drupal\master\Plugin\FieldFormType\InsuranceFieldFormType.
 */

namespace Drupal\master\Plugin\FieldFormType;

use Drupal\master\FieldFormTypeBase;

/**
 * Provides a 'InsuranceFieldFormType'.
 *
 * FieldFormType(
 *   id = "insurance_form",
 *   label = "Insurance form",
 * )
 */
class InsuranceFieldFormType extends FieldFormTypeBase {

  public function getFormElements() {
    $form['test'] = [
      '#markup' => 'Insurance form',
    ];
    return $form;
  }

}
