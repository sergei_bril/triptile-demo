<?php

/**
 * @file
 * Contains \Drupal\master\Plugin\FieldFormType\TransferFieldFormType.
 */

namespace Drupal\master\Plugin\FieldFormType;

use Drupal\master\FieldFormTypeBase;

/**
 * Provides a 'TransferFieldFormType'.
 *
 * FieldFormType(
 *   id = "transfer_form",
 *   label = "Transfer form",
 * )
 */
class TransferFieldFormType extends FieldFormTypeBase {

  public function getFormElements() {
    $form['test'] = [
      '#markup' => 'Transfer form',
    ];
    return $form;
  }

}
