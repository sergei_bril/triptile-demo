<?php

namespace Drupal\store\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\salesforce\Entity\MappableEntityInterface;
use Drupal\salesforce\Entity\MappableEntityTrait;
use Drupal\store\Price;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the Store order entity.
 *
 * @ingroup store
 *
 * @ContentEntityType(
 *   id = "store_order",
 *   label = @Translation("Store order"),
 *   bundle_label = @Translation("Store order type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\store\StoreOrderListBuilder",
 *     "views_data" = "Drupal\store\Entity\StoreOrderViewsData",
 *     "form" = {
 *       "default" = "Drupal\store\Form\StoreOrderForm",
 *       "add" = "Drupal\store\Form\StoreOrderForm",
 *       "edit" = "Drupal\store\Form\StoreOrderForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\master\Entity\EntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "store_order",
 *   data_table = "store_order",
 *   revision_table = "store_order_revision",
 *   revision_data_table = "store_order_field_revision",
 *   admin_permission = "administer store order entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "number",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/store/store-order/{store_order}",
 *     "add-page" = "/admin/store/store-order/add",
 *     "add-form" = "/admin/store/store-order/add/{store_order_type}",
 *     "edit-form" = "/admin/store/store-order/{store_order}/edit",
 *     "delete-form" = "/admin/store/store-order/{store_order}/delete",
 *     "collection" = "/admin/store/store-order",
 *   },
 *   bundle_entity_type = "store_order_type",
 *   field_ui_base_route = "entity.store_order_type.edit_form",
 *   settings_form = "Drupal\store\Form\StoreOrderSettingsForm"
 * )
 */
class StoreOrder extends ContentEntityBase implements StoreOrderInterface, MappableEntityInterface {

  use EntityChangedTrait;
  use MappableEntityTrait;

  /**
   * Order statuses.
   */
  const
    STATUS_NEW = 'new',
    STATUS_PROCESSING = 'processing',
    STATUS_MODIFICATION_REQUESTED = 'modification_requested',
    STATUS_CANCELLATION_REQUESTED = 'cancellation_requested',
    STATUS_BOOKED = 'booked',
    STATUS_MODIFIED = 'modified',
    STATUS_CANCELED = 'canceled',
    STATUS_FAILED = 'failed';

  /**
   * Order states.
   */
  const
    STATE_NEW = 1,
    STATE_PROCESSING = 2,
    STATE_BOOKED = 3,
    STATE_CANCELED = 4;

  /**
   * Mapping of order statuses to states.
   *
   * @var array
   */
  protected static $stateStatusMapping = [
    self::STATE_NEW => [self::STATUS_NEW, self::STATUS_FAILED],
    self::STATE_PROCESSING => [self::STATUS_PROCESSING, self::STATUS_MODIFICATION_REQUESTED, self::STATUS_CANCELLATION_REQUESTED],
    self::STATE_BOOKED => [self::STATUS_BOOKED, self::STATUS_MODIFIED],
    self::STATE_CANCELED => [self::STATUS_CANCELED],
  ];

  /**
   * Gets a status name or array of all status names keyed by it's value.
   *
   * @param string $status
   * @return array|string
   */
  public static function getStatusName($status = '') {
    $names = [
      static::STATUS_NEW => t('New'),
      static::STATUS_PROCESSING => t('Processing'),
      static::STATUS_MODIFICATION_REQUESTED => t('Modification requested'),
      static::STATUS_CANCELLATION_REQUESTED => t('Cancellation requested'),
      static::STATUS_BOOKED => t('Booked'),
      static::STATUS_MODIFIED => t('Modified'),
      static::STATUS_CANCELED => t('Canceled'),
      static::STATUS_FAILED => t('Failed'),
    ];

    return isset($names[$status]) ? $names[$status] : $names;
  }

  public static function getStateName($state = 0) {
    $names = [
      static::STATE_NEW => t('New'),
      static::STATE_PROCESSING => t('Processing'),
      static::STATE_BOOKED => t('Booked'),
      static::STATE_CANCELED => t('Canceled'),
    ];

    return isset($names[$state]) ? $names[$state] : $names;
  }

  /**
   * Gets a status description or array of all status descriptions keyed by
   * it's value.
   *
   * @param string $status
   * @return array|string
   */
  public function getStatusDescription($status = '') {
    $descriptions = [
      static::STATUS_NEW => t('This is a new order. Thank you for using our service.'),
      static::STATUS_MODIFICATION_REQUESTED => t('Your order is being modified. Once modification is complete, you will be notified by email.'),
      static::STATUS_CANCELLATION_REQUESTED => t( 'Your order is being canceled. Once cancellation is complete, you will be notified by email.'),
      static::STATUS_BOOKED => t('Your order is booked. Please download your tickets in PDF format from this page.'),
      static::STATUS_MODIFIED => t('Your order has been modified. Thank you for using our service.'),
      static::STATUS_CANCELED => t('Your order has been canceled. Thank you for using our service.'),
      static::STATUS_FAILED => t('Your order payment has been failed. You can try to pay again.')
    ];

    if (!$this->getTicketIssueDate()) {
      $descriptions[static::STATUS_PROCESSING] = t('We received your order. Your tickets will be issued within 1 business day and sent to your email.');
    }
    else {
      $descriptions[static::STATUS_PROCESSING] = t('We received your order. Your tickets will be issued about @ticket_issue_date and sent to your email.', ['@ticket_issue_date' => $this->getTicketIssueDate()->format(DATETIME_DATE_STORAGE_FORMAT)]);
    }

    return isset($descriptions[$status]) ? $descriptions[$status] : $descriptions;
  }

    /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $owner) {
    $this->set('owner', $owner->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('owner', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('owner')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderNumber() {
    return $this->get('number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderNumber($number) {
    $this->set('number', $number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSiteCode($code) {
    $this->set('site', $code);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteCode() {
    return $this->get('site')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    foreach (static::$stateStatusMapping as $state => $statuses) {
      if (in_array($this->getStatus(), $statuses)) {
        return $state;
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getHash() {
    return $this->get('hash')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHash($hash) {
    $this->set('hash', $hash);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTripType() {
    return $this->get('trip_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTripType($trip_type) {
    $this->set('trip_type', $trip_type);
    return $this;
  }

  /**
   * @todo find a better way to set multiple field values
   *
   * {@inheritdoc}
   */
  public function setTickets($train_tickets) {
    foreach ($train_tickets as $train_ticket) {
      $this->ticket[] = $train_ticket;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    parent::postCreate($storage);

    // @todo Set current site code.
    $this->setSiteCode('RN');

    $this->setStatus(static::STATUS_NEW);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->setNewRevision(true);

    // Set hash
    if (!$this->getHash()) {
      $this->setHash(md5($this->getOrderNumber() . time()));
    }

    // Set predefined name of PDF file.
    // @todo Create custom element and field widget that will allow to change file names and uses predefined template for file name.
    if (!empty($this->getPdfFiles())) {
      $fileName = $this->getPdfFileName();
      foreach ($this->getPdfFiles() as $delta => $file) {
        if ($file->getFilename() != $fileName) {
          $file->setFilename($fileName);
          $newDestination = \Drupal::service('file_system')->dirname($file->getFileUri()) . '/' . $fileName;
          if ($newFile = file_move($file, $newDestination)) {
            $this->pdf_file[$delta] = $newFile->id();
          }
        }
      }
    }
  }

  /**
   * Generates PDF file name for current order.
   *
   * @return string
   */
  protected function getPdfFileName() {
    $name = '';
    if ($this->bundle() == 'train_order') {
      $depStation = $arrStation = null;
      foreach ($this->getTickets() as $ticket) {
        if (is_null($depStation) || ($depStation->id() != $ticket->getDepartureStation()->id() && $arrStation->id() != $ticket->getArrivalStation()->id())) {
          if (!is_null($depStation) && $depStation->id() != $ticket->getDepartureStation()->id() && $arrStation->id() != $ticket->getArrivalStation()->id()) {
            $name .= '--';
          }
          $depStation = $ticket->getDepartureStation();
          $arrStation = $ticket->getArrivalStation();
          $depCity = $depStation->getParentStation() ? : $depStation;
          $arrCity = $arrStation->getParentStation() ? : $arrStation;
          $name .= $depCity . '-' . $arrCity . '-' . $ticket->getDepartureDateTime()->format('Y-m-d');
        }
      }
    }

    return $name . '.pdf';
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Order number'))
      ->setDescription(t('The order number of the Store order entity.'))
      ->setRevisionable(TRUE)
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['hash'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hash'))
      ->setDescription(t('Unique order hash'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 40,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['owner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The user ID of owner.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['order_total'] = BaseFieldDefinition::create('price')
      ->setLabel(t('Order total'))
      ->setDescription(t('The order total.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'price_default',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'price_default',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['site'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Site'))
      ->setDescription(t('Site.'))
      ->setRevisionable(TRUE)
      ->setSettings(array(
        'allowed_values' => [
          'RN' => 'RN',
          'RT' => 'RN',
        ]
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'list_default',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['internal_note'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Internal note'))
      ->setDescription(t('Long text field for internal use.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textarea',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDescription(t('Status.'))
      ->setRevisionable(TRUE)
      ->setSettings(['allowed_values' => static::getStatusName()])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'list_default',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel('Data');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Generates order number for the order.
   *
   * @return string
   */
  protected function generateOrderNumber() {
    // @todo Use configurable template.
    return time();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderTotal() {
    return $this->get('order_total')->first()->toPrice();
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderTotal(Price $total) {
    $this->set('order_total', $total);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTickets() {
    $tickets = [];
    if ($this->bundle() == 'train_order') {
      // @TODO: find a way to work with special fields. Search same using in the project.
      $tickets = $this->get('ticket')->referencedEntities();
    }
    return $tickets;
  }

  /**
   * {@inheritdoc}
   */
  public function getInvoices() {
    $ids = \Drupal::entityQuery('invoice')
      ->condition('order_reference.target_id', $this->id())
      ->sort('id')
      ->execute();
    return \Drupal::entityTypeManager()
      ->getStorage('invoice')
      ->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getPdfFiles() {
    $files = [];
    if ($this->bundle() == 'train_order') {
      $files = $this->get('pdf_file')->referencedEntities();
    }
    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function getHtmlClass($value = '') {
    return strtolower(Html::cleanCssIdentifier($value));
  }

  /**
   * {@inheritdoc}
   */
  public function getNotes() {
    $notes = '';
    if ($this->bundle() == 'train_order') {
      $notes = $this->get('user_note')->value;
    }
    return $notes;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotes($notes) {
    if ($this->bundle() == 'train_order') {
      $this->set('user_note', $notes);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTicketIssueDate() {
    if (!empty($this->get('ticket_issue_date')->first())) {
      return $this->get('ticket_issue_date')->first()->date;
    }

    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function setTicketIssueDate(DrupalDateTime $ticket_issue_date) {
    $this->set('ticket_issue_date', $ticket_issue_date->format(DATETIME_DATE_STORAGE_FORMAT));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderItems() {
    return \Drupal::entityTypeManager()->getStorage('order_item')->loadByProperties(['order_reference' => $this->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getData($key) {
    if (!empty($this->get('data')->first()) && !empty($this->get('data')->first()->getValue()[$key])) {
      return $this->get('data')->first()->getValue()[$key];
    }
    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function setData($key, $value) {
    $data = [];
    if (!empty($this->get('data')->first())) {
      $data = $this->get('data')->first()->getValue();
    }
    $data = array_merge($data, [$key => $value]);
    $this->set('data', [$data]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeData($key) {
    if (!empty($this->get('data')->first())) {
      $data = $this->get('data')->first()->getValue();
      unset($data[$key]);
      $this->set('data', [$data]);
    }
    return $this;
  }


}
