<?php

/**
 * Implements hook_drush_command().
 */
function salesforce_drush_command() {
  $items = [];
  $items['salesforce-sync'] = [
    'description' => 'Launch sync on all triggered mapping objects.',
    'drupal_dependencies' => ['salesforce'],
    'aliases' => ['sf-sync'],
  ];

  $items['salesforce-pull'] = [
    'description' => 'Pull records from salesforce',
    'options' => [
      'salesforce-object' => 'A salesforce object name that has to be pulled.'
    ],
    'drupal_dependencies' => ['salesforce'],
    'aliases' => ['sf-pull'],
  ];

  return $items;
}

function drush_salesforce_sync() {
  \Drupal::service('salesforce_sync')->processSync();
}

function drush_salesforce_pull() {
  \Drupal::service('salesforce_sync')->triggerRecordsSync(drush_get_option('salesforce_object', null));
}
