<?php

namespace Drupal\rn_user;

use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;

class UserCountryCacheContext implements CacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('User country');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if (function_exists('geoip_country_code_by_name')) {
      return geoip_country_code_by_name(\Drupal::request()->getClientIp());
    }
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }
}