<?php

namespace Drupal\master;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Base class for Field form type plugins.
 */
abstract class FieldFormTypeBase extends PluginBase implements FieldFormTypeInterface {

  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElements() {
    $form = [];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePassengerForm(array $form, FormStateInterface $form_state) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPassengerForm(array $form, FormStateInterface $form_state) {
    return $this;
  }
}
