(function ($) {
  Drupal.behaviors.rnUserBlockBehavior = {
    attach: function (context, settings) {

      var indicator_class = 'opened';
      var $trigger = $('.user-settings');
      var $triangle = $('.user-triangle');
      var $block = $('.user-blocks');
      var maxWidth = 768;

      if($(window).width() >= maxWidth) {
        $trigger.once('menuTriggerClick').click(function () {
          if ($(this).parent().hasClass(indicator_class)) {
            $block.fadeOut();
            $triangle.fadeOut();
          }
          else {
            $block.fadeIn();
            $triangle.fadeIn();
          }
          $(this).parent().toggleClass(indicator_class);
        });

        $('html').once('menuHtmlClick').click(function (e) {
          var target = $(e.target);

          if (!target.closest(".user-block").length) {
            $block.fadeOut();
            $triangle.fadeOut();
            $(this).parent().toggleClass(indicator_class);
          }
        });
      }
      else {
        $('.user-blocks .block__title').once('blockTitleClick').click(function(){
          $('.user-blocks .block__content').hide();
          $(this).next('.block__content').toggle();
        });
      }
    }
  };

})(jQuery);