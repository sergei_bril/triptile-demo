<?php

namespace Drupal\rn_user\Form;

use Drupal\user\ProfileForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\salesforce\Form\SalesforceSyncEntityFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\salesforce\SalesforceSync;
use Drupal\salesforce\Plugin\SalesforceMappingManager;

class UserForm extends ProfileForm {

  use SalesforceSyncEntityFormTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity query factory service.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * @var \Drupal\salesforce\SalesforceSync
   */
  protected $salesforceSync;

  /**
   * @var \Drupal\salesforce\Plugin\SalesforceMappingManager
   */
  protected $mappingManager;

  /**
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query factory.
   * @param \Drupal\salesforce\SalesforceSync $salesforce_sync
   * @param \Drupal\salesforce\Plugin\SalesforceMappingManager $mapping_manager
   */
  public function __construct(EntityManagerInterface $entity_manager, LanguageManagerInterface $language_manager, QueryFactory $entity_query, SalesforceSync $salesforce_sync, SalesforceMappingManager $mapping_manager) {
    parent::__construct($entity_manager, $language_manager, $entity_query);
    $this->salesforceSync = $salesforce_sync;
    $this->mappingManager = $mapping_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('language_manager'),
      $container->get('entity.query'),
      $container->get('salesforce_sync'),
      $container->get('plugin.manager.salesforce_mapping')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $entity = $this->entity;
    $this->salesforceBaseTrigger($form_state, $entity);
  }

}