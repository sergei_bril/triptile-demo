<?php

namespace Drupal\master;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface for Field form type plugins.
 */
interface FieldFormTypeInterface extends PluginInspectionInterface {

  /**
   * Gets Passsenger field form type form.
   *
   * @return array
   */
  public function getFormElements();

  /**
   * Process validation on passenger field form type form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return static
   */
  public function validatePassengerForm(array $form, FormStateInterface $form_state);

  /**
   * Submit passenger field form type form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return static
   */
  public function submitPassengerForm(array $form, FormStateInterface $form_state);

}
