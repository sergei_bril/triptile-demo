<?php

namespace Drupal\payment\Plugin\PaymentAdapter;

use Drupal\payment\API\PaypalAPI;
use Drupal\payment\Entity\Transaction;
use Drupal\payment\Entity\TransactionInterface;

/**
 * Class PaypalWPP
 *
 * @PaymentAdapter(
 *   id = "paypal_wpp",
 *   label = @Translation("Paypal WPP")
 * )
 */
class PaypalWPP extends PaypalBase implements OnSitePaymentAdapterInterface {

  /**
   * {@inheritdoc}
   */
  public function doPayment(TransactionInterface $transaction, array $payment_data, array $billing_data) {
    $transaction->setAmount($this->calculateAmount($transaction));
    $result = $this->getAPI()
      ->setConfig($this->configuration)
      ->setTransaction($transaction)
      ->setCreditCardData($payment_data)
      ->setBillingProfile($transaction->getInvoice()->getCustomerProfile())
      ->doDirectPayment();

    if (in_array($result['ACK'], [PaypalAPI::ACK_SUCCESS, PaypalAPI::ACK_SUCCESS_WITH_WARNING])) {
      $transaction->setRemoteId($result['TRANSACTIONID'])
        ->setRemoteStatus($result['ACK'])
        ->setStatus(Transaction::STATUS_SUCCESS)
        ->appendMessage('Payment processed successfully.');
    }
    elseif (in_array($result['ACK'], [PaypalAPI::ACK_FAILURE, PaypalAPI::ACK_FAILURE_WITH_WARNING])) {
      $transaction->setRemoteStatus($result['ACK'])
        ->setStatus(Transaction::STATUS_FAILED)
        ->appendMessage('Payment failed.');
      if (isset($result['ACK'])) {
        $transaction->appendMessage('Error code: ' . $result['L_ERRORCODE0'] . "\n" . $result['L_SHORTMESSAGE0']
          . "\n" . $result['L_LONGMESSAGE0']);
      }
      if (isset($result['TRANSACTIONID'])) {
        $transaction->setRemoteId($result['TRANSACTIONID']);
      }
    }
  }

}
