<?php

namespace Drupal\rn_user\Entity;

use Drupal\salesforce\Entity\MappableEntityInterface;
use Drupal\salesforce\Entity\MappableEntityTrait;
use Drupal\user\Entity\User as UserBase;

/**
 * Class User
 *
 * Replaces Drupal core user entity class in order to add custom methods.
 *
 * @package Drupal\rn_user\Entity
 */
class User extends UserBase implements MappableEntityInterface {

  use MappableEntityTrait;

  /**
   * Gets the latest user customer profile.
   *
   * @return \Drupal\store\Entity\CustomerProfile|null
   */
  public function getCustomerProfile() {
    if (\Drupal::moduleHandler()->moduleExists('store')) {
      $profiles = \Drupal::entityTypeManager()
        ->getStorage('customer_profile')
        ->loadByProperties(['uid.target_id' => $this->id()]);
      ksort($profiles);
      return end($profiles);
    }

    return null;
  }

}
