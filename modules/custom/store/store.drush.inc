<?php
/**
 * @file Contains the code to generate the custom drush commands.
 */
/**
 * Implements hook_drush_command().
 */
function store_drush_command() {
  $items = array();
  $items['update_currency_rates'] = [
    'description' => 'Update currency rates',
    'drupal dependencies' => ['store'],
    'aliases' => ['update-currency-rates', 'ucr'],
  ];
  return $items;
}
/**
 * Call back function drush_store_update_currency_rates()
 */
function drush_store_update_currency_rates() {
  $rates_updater = \Drupal::service('store.currency_fixed_rate_updater');
  $rates_updater->updateRates();
  drush_print('Currency rates has been update successfully');
}