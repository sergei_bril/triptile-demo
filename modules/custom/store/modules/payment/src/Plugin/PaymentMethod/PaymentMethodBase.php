<?php

namespace Drupal\payment\Plugin\PaymentMethod;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\payment\Entity\Merchant;
use Drupal\payment\Entity\Transaction;
use Drupal\payment\Entity\TransactionInterface;
use Drupal\payment\Plugin\PaymentAdapter\OffSitePaymentAdapterInterface;
use Drupal\payment\Plugin\PaymentAdapter\OnSitePaymentAdapterInterface;
use Drupal\payment\Plugin\PaymentAdapter\RemoteBillingProfileAdapterInterface;
use Drupal\rn_user\Entity\User;
use Drupal\store\Entity\CustomerProfile;
use Drupal\store\Entity\Invoice;
use Drupal\store\Entity\InvoiceInterface;
use Masterminds\HTML5\Exception;
use Symfony\Component\HttpFoundation\Request;
use Drupal\salesforce\SalesforceSync;
use Drupal\salesforce\Plugin\SalesforceMappingManager;
use Drupal\salesforce\Entity\MappableEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Base class for Payment method plugins.
 */
abstract class PaymentMethodBase extends PluginBase implements PaymentMethodInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Url
   */
  protected $successUrl;

  /**
   * @var \Drupal\Core\Url
   */
  protected $cancelUrl;

  /**
   * @var \Drupal\Core\Url
   */
  protected $failUrl;

  /**
   * @var array
   */
  protected $paymentData = [];

  /**
   * @var array
   */
  protected $billingData = [];

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * @var \Drupal\salesforce\SalesforceSync
   */
  protected $salesforceSync;

  /**
   * @var \Drupal\salesforce\Plugin\SalesforceMappingManager
   */
  protected $mappingManager;


  /**
   * PaymentMethodBase constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // @todo Replace with dependency injection
    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->entityQuery = \Drupal::service('entity.query');
    $this->salesforceSync = \Drupal::service('salesforce_sync');
    $this->mappingManager = \Drupal::service('plugin.manager.salesforce_mapping');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'label' => $this->getPluginDefinition()['label'],
      'weight' => 0,
      'status' => 1,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    // @todo Implement
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('Label visible to users.'),
      '#default_value' => $this->configuration['label'],
      '#required' => true,
    ];

    $form['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#default_value' => $this->getWeight(),
      '#delta' => 10,
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => $this->isEnabled(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement validateConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['label'] = $values['label'];
      $this->configuration['weight'] = $values['weight'];
      $this->configuration['status'] = (bool) $values['status'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->configuration['weight'];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return $this->configuration['status'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->configuration['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentDataForm(array $form, FormStateInterface $form_state, $include_title = false) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildBillingProfileForm(array $form, FormStateInterface $form_state, $include_title = false) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaymentDataForm(array $form, FormStateInterface $form_state) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function validateBillingProfileForm(array $form, FormStateInterface $form_state) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaymentDataForm(array $form, FormStateInterface $form_state) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function submitBillingProfileForm(array $form, FormStateInterface $form_state) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSuccessUrl(Url $success_url) {
    $this->successUrl = $success_url;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCancelUrl(Url $cancel_url) {
    $this->cancelUrl = $cancel_url;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setFailUrl(Url $fail_url) {
    $this->failUrl = $fail_url;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentData(array $payment_data) {
    $this->paymentData = $payment_data;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setBillingData(array $billing_data) {
    $this->billingData = $billing_data;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function processPayment(InvoiceInterface $invoice) {
    $url = $this->failUrl;
    $this->createBillingProfile($invoice);

    foreach ($this->getMerchants($invoice) as $merchant) {
      try {
        $payment_adapter = $merchant->getPaymentAdapterPlugin();
        $transaction = $this->createTransaction($invoice, $merchant);

        if ($payment_adapter instanceof OnSitePaymentAdapterInterface) {
          $payment_adapter->doPayment($transaction, $this->paymentData, $this->billingData);
          if (in_array($transaction->getStatus(), [Transaction::STATUS_PENDING, Transaction::STATUS_SUCCESS])) {
            $url = $this->successUrl;
          }
        }

        if ($payment_adapter instanceof OffSitePaymentAdapterInterface) {
          $payment_adapter->setSuccessUrl($this->successUrl)
            ->setCancelUrl($this->cancelUrl)
            ->setFailUrl($this->failUrl);
          if ($payment_adapter->initPayment($transaction, $this->paymentData, $this->billingData)) {
            $url = $payment_adapter->getPaymentUrl();
          }
        }

        $transaction->save();
        $this->updateInvoiceStatus($invoice, $transaction);
      }
      catch (\Exception $e) {
        watchdog_exception('payment', $e);
      }

      if (in_array($invoice->getStatus(), [Invoice::STATUS_CLEARING, Invoice::STATUS_PAID])) {
        // exit if transaction has been processed successfully
        break;
      }
    }

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function paymentReturned(InvoiceInterface $invoice) {
    if ($invoice->getStatus() == Invoice::STATUS_CLEARING && $transaction = $this->getLatestTransaction($invoice)) {
      try {
        $adapter = $transaction->getMerchant()
          ->getPaymentAdapterPlugin();
        if ($adapter instanceof OffSitePaymentAdapterInterface) {
          $adapter->completePayment($transaction);
        }
        if ($adapter instanceof OnSitePaymentAdapterInterface && $transaction->getStatus() == Transaction::STATUS_PENDING) {
          $adapter->syncTransactionStatus($transaction);
        }
        if ($adapter instanceof RemoteBillingProfileAdapterInterface) {
          $this->setBillingData($adapter->getBillingProfileData($transaction))
            ->createBillingProfile($invoice);
        }

        $transaction->save();
        $this->updateInvoiceStatus($invoice, $transaction);
      }
      catch (\Exception $e) {
        watchdog_exception('payment', $e);
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function paymentCanceled(InvoiceInterface $invoice) {
    if ($transaction = $this->getLatestTransaction($invoice)) {
      try {
        $transaction->getMerchant()
          ->getPaymentAdapterPlugin()
          ->syncTransactionStatus($transaction);
        $this->updateInvoiceStatus($invoice, $transaction);
      }
      catch (Exception $e) {
        watchdog_exception('payment', $e);
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function paymentFailed(InvoiceInterface $invoice) {
    if($transaction = $this->getLatestTransaction($invoice)) {
      try {
        $transaction->getMerchant()
          ->getPaymentAdapterPlugin()
          ->syncTransactionStatus($transaction);
        $this->updateInvoiceStatus($invoice, $transaction);
      }
      catch (\Exception $exception) {
        watchdog_exception('payment', $exception);
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function invoiceIsPaid(InvoiceInterface $invoice) {
    $paid = false;
    if ($invoice->getStatus() == Invoice::STATUS_PAID) {
      $paid = true;
    }
    elseif($transaction = $this->getLatestTransaction($invoice)) {
      if ($transaction->getStatus() == Transaction::STATUS_PENDING) {
        try {
          $transaction->getMerchant()
            ->getPaymentAdapterPlugin()
            ->syncTransactionStatus($transaction);
        }
        catch (\Exception $exception) {
          watchdog_exception('payment', $exception);
        }
      }

      $this->updateInvoiceStatus($invoice, $transaction);
      $paid = in_array($invoice->getStatus(), [Invoice::STATUS_CLEARING, Invoice::STATUS_PAID]);
    }

    return $paid;
  }

  /**
   * {@inheritdoc}
   */
  public function processTransactionUpdateRequest(TransactionInterface $transaction, Request $request) {
    $transaction->getMerchant()
      ->getPaymentAdapter()
      ->processTransactionUpdateRequest($transaction, $request);

    return $this;
  }

  /**
   * Loads merchants for the current payment method.
   *
   * @param \Drupal\store\Entity\InvoiceInterface $invoice
   * @return \Drupal\payment\Entity\Merchant[]
   */
  protected function getMerchants(InvoiceInterface $invoice) {
    // @todo Use merchant router service to load merchants

    $merchants = [];
    /** @var \Drupal\payment\Entity\Merchant $merchant */
    foreach ($this->entityTypeManager->getStorage('merchant')->loadMultiple() as $merchant) {
      if (in_array($this->getBaseId(), $merchant->getPaymentMethods()) && $merchant->isEnabled()) {
        $merchants[] = $merchant;
      }
    }

    return $merchants;
  }

  /**
   * Creates new transaction and sets default values.
   *
   * @param \Drupal\store\Entity\InvoiceInterface $invoice
   * @param \Drupal\payment\Entity\Merchant $merchant
   * @return \Drupal\payment\Entity\Transaction
   */
  protected function createTransaction(InvoiceInterface $invoice, Merchant $merchant) {
    /** @var \Drupal\payment\Entity\Transaction $transaction */
    $transaction = Transaction::create()
      ->setMerchant($merchant)
      ->setOriginalAmount($invoice->getAmount())
      ->setType(Transaction::TYPE_PAYMENT)
      ->setPaymentMethod($this->getBaseId())
      ->setInvoice($invoice);
    $transaction->save();

    return $transaction;
  }

  /**
   * Maps transactions status to invoice. Saves invoice if status changes.
   *
   * @param \Drupal\store\Entity\InvoiceInterface $invoice
   * @param \Drupal\payment\Entity\TransactionInterface $transaction
   * @return static
   */
  protected function updateInvoiceStatus(InvoiceInterface $invoice, TransactionInterface $transaction) {
    $old_status = $invoice->getStatus();
    switch ($transaction->getStatus()) {
      case Transaction::STATUS_SUCCESS:
        $invoice->setStatus(Invoice::STATUS_PAID);
        break;
      case Transaction::STATUS_PENDING:
        $invoice->setStatus(Invoice::STATUS_CLEARING);
        break;
      case Transaction::STATUS_FAILED:
        $invoice->setStatus(Invoice::STATUS_UNPAID);
        break;
    }

    if ($invoice->getStatus() != $old_status) {
      $invoice->save();
    }

    return $this;
  }

  /**
   * Loads latest created transaction on the invoice.
   *
   * @param \Drupal\store\Entity\InvoiceInterface $invoice
   * @return \Drupal\payment\Entity\Transaction|null
   */
  protected function getLatestTransaction(InvoiceInterface $invoice) {
    $transaction = null;
    $ids = $this->entityQuery->get('transaction', 'AND')
      ->condition('invoice.target_id', $invoice->id())
      ->condition('type', Transaction::TYPE_PAYMENT)
      ->condition('payment_method', $this->getBaseId())
      ->execute();
    if (!empty($ids)) {
      $transaction = $this->entityTypeManager->getStorage('transaction')
        ->load(max($ids));
    }

    return $transaction;
  }

  /**
   * Creates customer billing profile.
   *
   * @param \Drupal\store\Entity\InvoiceInterface $invoice
   * @return static
   */
  protected function createBillingProfile(InvoiceInterface $invoice) {
    if (!empty($this->billingData)) {
      // If there is no user with the specified email then set invoice owner as
      // new customer profile owner.
      $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['mail' => $this->billingData['email']]);
      if (!$user = reset($users)) {
        $user = $invoice->getUser();
      }
      if (!$user) {
        $random = new Random();
        $user = User::create([
          'name' => $this->billingData['email'],
          'mail' => $this->billingData['email'],
          'pass' => $random->string(8),
          'status' => 1,
        ]);
      }
      CustomerProfile::create()
        ->setInvoice($invoice)
        ->setAddress($this->billingData)
        ->setPhoneNumber($this->billingData['phone_number'])
        ->setEmail($this->billingData['email'])
        ->setOwner($user)
        ->save();
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function triggerSalesforceEntities(Invoice $invoice) {
    $this->salesforceBaseTrigger($invoice);
    if ($transaction = $this->getLatestTransaction($invoice)) {
      $this->salesforceBaseTrigger($transaction);
    }
    if ($user = $invoice->getUser()) {
      $this->salesforceBaseTrigger($user);
    }
  }

  /**
   * Trigger synchronize action 'PUSH' for entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface
   */
  protected function salesforceBaseTrigger(ContentEntityInterface $entity) {
    if (($plugin_id = $this->mappingManager->findPluginForEntityType($entity->getEntityTypeId()))
      && $entity instanceof MappableEntityInterface && !$entity->isPullProcessing()) {
      $definition = $this->mappingManager->getDefinition($plugin_id);
      if (in_array(SalesforceSync::OPERATION_UPDATE, $definition['entity_operations'])) {
        $this->salesforceSync->setTriggerForEntity(SalesforceSync::SYNC_ACTION_PUSH, $entity->id(), $entity->getEntityTypeId());
      }
    }
  }

}
