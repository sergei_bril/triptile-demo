<?php

namespace Drupal\payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\payment\Plugin\PaymentMethodManager;
use Drupal\store\Entity\Invoice;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class PaymentServiceUrlController
 *
 * Serves cancel and return payment urls.
 *
 * @package Drupal\payment\Controller
 */
class PaymentServiceUrlController extends ControllerBase {

  /**
   * @var \Drupal\payment\Plugin\PaymentMethodManager
   */
  protected $paymentMethodManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * PaymentServiceUrlController constructor.
   *
   * @param \Drupal\payment\Plugin\PaymentMethodManager $payment_method_manager
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   */
  public function __construct(PaymentMethodManager $payment_method_manager, EntityTypeManager $entity_type_manager) {
    $this->paymentMethodManager = $payment_method_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.payment.payment_method'),
      $container->get('entity_type.manager')
    );
  }

  public function success($invoice, $payment_method) {
    $invoice = $this->entityTypeManager->getStorage('invoice')->load($invoice);
    $config = $this->config('plugin.plugin_configuration.payment_method.' . $payment_method)->get();
    /** @var \Drupal\payment\Plugin\PaymentMethod\PaymentMethodBase $plugin */
    $plugin = $this->paymentMethodManager->createInstance($payment_method, $config);
    $plugin->paymentReturned($invoice);

    if ($plugin->invoiceIsPaid($invoice)) {
      $plugin->triggerSalesforceEntities($invoice);
      return $this->doPaidRedirect($invoice);
    }
    else {
      $plugin->triggerSalesforceEntities($invoice);
      return $this->doFailedRedirect($invoice);
    }
  }

  public function cancel($invoice, $payment_method) {
    $invoice = $this->entityTypeManager->getStorage('invoice')->load($invoice);
    $config = $this->config('plugin.plugin_configuration.payment_method.' . $payment_method)->get();
    /** @var \Drupal\payment\Plugin\PaymentMethod\PaymentMethodBase $plugin */
    $plugin = $this->paymentMethodManager->createInstance($payment_method, $config);
    $plugin->paymentCanceled($invoice);
    $plugin->triggerSalesforceEntities($invoice);
    return $this->doCancelRedirect($invoice);
  }

  public function fail($invoice, $payment_method) {
    $invoice = $this->entityTypeManager->getStorage('invoice')->load($invoice);
    $config = $this->config('plugin.plugin_configuration.payment_method.' . $payment_method)->get();
    /** @var \Drupal\payment\Plugin\PaymentMethod\PaymentMethodBase $plugin */
    $plugin = $this->paymentMethodManager->createInstance($payment_method, $config);
    $plugin->paymentFailed($invoice);
    $plugin->triggerSalesforceEntities($invoice);
    return $this->doFailedRedirect($invoice);
  }

  protected function doPaidRedirect(Invoice $invoice) {
    drupal_set_message($this->t('Invoice has been paid.'));
    return $this->redirect('entity.invoice.user_view', ['invoice_number' => $invoice->id()]);
  }

  protected function doFailedRedirect(Invoice $invoice) {
    drupal_set_message($this->t('Payment failed.'), 'error');
    return $this->redirect('entity.invoice.payment', ['invoice_number' => $invoice->id()]);
  }

  protected function doCancelRedirect(Invoice $invoice) {
    drupal_set_message($this->t('You canceled the payment.'), 'warning');
    return $this->redirect('entity.invoice.payment', ['invoice_number' => $invoice->id()]);
  }

}
