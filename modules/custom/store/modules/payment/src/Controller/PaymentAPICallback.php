<?php

namespace Drupal\payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\payment\Entity\Transaction;
use Drupal\payment\Plugin\PaymentMethodManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PaymentAPICallback extends ControllerBase {

  /**
   * @var \Drupal\payment\Plugin\PaymentMethodManager
   */
  protected $paymentMethodManager;

  /**
   * PaymentAPICallback constructor.
   * @param \Drupal\payment\Plugin\PaymentMethodManager $payment_method_manager
   */
  public function __construct(PaymentMethodManager $payment_method_manager) {
    $this->paymentMethodManager = $payment_method_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('plugin.manager.payment.payment_method'));
  }

  public function paypalIPN(Transaction $transaction, Request $request) {
    $config = $this->config('plugin.plugin_configuration.payment_method.' . $transaction->getPaymentMethod())->get();
    /** @var \Drupal\payment\Plugin\PaymentMethod\PaymentMethodBase $payment_method */
    $payment_method = $this->paymentMethodManager->createInstance($transaction->getPaymentMethod(), $config);
    $payment_method->processTransactionUpdateRequest($transaction, $request);

    return new Response();
  }

}
